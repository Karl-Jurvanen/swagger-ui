# Testing swagger-ui in Mkdocs


### Embedded Swagger-ui

Some text here. 123 123

<div id="swagger-ui"></div>

<script>
window.onload = function() {
  var URL = `${window.location.origin}${window.location.pathname}swagger/swagger.yaml`
  const ui = SwaggerUIBundle({
    url: URL,
    //url: "http://petstore.swagger.io/v2/swagger.json",
    dom_id: '#swagger-ui',
    presets: [
      SwaggerUIBundle.presets.apis,
      SwaggerUIStandalonePreset
    ]
  })

  window.ui = ui
}
</script>